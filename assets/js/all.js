//mobile menu
document.querySelector('#menu-mobile').addEventListener('click', function() {
    document.querySelector('#header-nav').style.display = 'none';
    document.querySelector('#header-nav-mobile').style.display = 'block';
});
document.querySelector('#close-menu').addEventListener('click', function() {
    document.querySelector('#header-nav').style.display = 'block';
    document.querySelector('#header-nav-mobile').style.display = 'none';
});


//Insert Images

var imgs = [
    'assets/image/products/aging-potion.png',
    'assets/image/products/bulgeye-potion.png',
    'assets/image/products/dragon-tonic.png',
    'assets/image/products/love-potion.png',
    'assets/image/products/polyjuice-potion.png',
    'assets/image/products/sleeping-draught.png'
];

for (i = 0; i < imgs.length; i++) {
    document.querySelector('.product-1').innerHTML = "<a href='#'id='btn-modal'><img src=" + imgs[0] + " /></a>";
    document.querySelector('.product-2').innerHTML = "<a href='#'id='btn-modal'><img src=" + imgs[1] + " /></a>";
    document.querySelector('.product-3').innerHTML = "<a href='#'id='btn-modal'><img src=" + imgs[2] + " /></a>";
    document.querySelector('.product-4').innerHTML = "<a href='#'id='btn-modal'><img src=" + imgs[3] + " /></a>";
    document.querySelector('.product-5').innerHTML = "<a href='#'id='btn-modal'><img src=" + imgs[4] + " /></a>";
    document.querySelector('.product-6').innerHTML = "<a href='#'id='btn-modal'><img src=" + imgs[5] + " /></a>";
};

dataJ = '';

function fetchJSONFile(path, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                var data = JSON.parse(httpRequest.responseText);
                if (callback) callback(data);
            }
        }
    };
    httpRequest.open('GET', path, false);
    httpRequest.send();
}

fetchJSONFile('https://api.myjson.com/bins/1ckqu7', function(data) {
    dataJ = data;
});

// Open Infos Json

var button = document.querySelector("#btn-modal");
var modal = document.querySelector('#myModal');
var close = document.querySelector(".close");


button.addEventListener("click", function(path, callback) {
    document.querySelector('.product-1').dataset.id = "1";
    document.querySelector(".info-product").innerHTML = +dataJ.potions[1].name;
    modal.style.display = "block";
}, false);
button.addEventListener("click", function(e) {
    document.querySelector('.product-2').dataset.id = "2";
    modal.style.display = "block";
}, false);
button.addEventListener("click", function(e) {
    document.querySelector('.product-3').dataset.id = "3";
    modal.style.display = "block";
}, false);
button.addEventListener("click", function(e) {
    document.querySelector('.product-4').dataset.id = "4";
    modal.style.display = "block";
}, false);
button.addEventListener("click", function(e) {
    document.querySelector('.product-5').dataset.id = "5";
    modal.style.display = "block";
}, false);
button.addEventListener("click", function(e) {
    document.querySelector('.product-6').dataset.id = "6";
    modal.style.display = "block";
}, false);

close.addEventListener("click", function(e) {
    modal.style.display = "none";
}, false);