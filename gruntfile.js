module.exports = function(grunt) {
    grunt.initConfig({
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'src/image',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'assets/image'
                }]
            }
        },

        uglify: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'assets/js',
                    src: ['*.js', '!*.min.js'],
                    dest: 'assets/js',
                    ext: '.min.js'
                }]
            }
        },

        less: {
            dist: {
                files: {
                    'assets/css/all.css': [ // <== Write here the path to the compiled CSS
                        'src/less/all.less' // <== Write here the path to the LESS file(s)
                    ]
                }
            }
        },

        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'assets/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'assets/css',
                    ext: '.min.css'
                }]
            }
        },

        watch: {
            styles: {
                files: ['src/**/*.less', 'assets/**/*.js'], // which files to watch
                tasks: ['uglify', 'less', 'cssmin'],
                options: {
                    nospawn: true
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');


    grunt.registerTask('default', ['watch']);
    grunt.registerTask('imagemin', ['imagemin']);
};